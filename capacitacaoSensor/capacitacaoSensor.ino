#include <ros.h>
#include <std_msgs/Float32.h>
#include <Adafruit_BMP085.h>
#include <Wire.h>

//Define o modelo do sensor e o nome pelo qual ele sera chamado
Adafruit_BMP085 bmp180;

//O bmp180 deve ser ligado sempre no 3.3v
// o pino scl vai na porta A5 e o pino sda vai na porta A4 do arduino

//Define o tipo de menssagem a ser publicada
std_msgs::Float32 temperatura1;

//Cria o publisher
ros::Publisher temp1("temperatura1", &temperatura1);

//Inicia o ROS
ros::NodeHandle nh;

void setup() {
  //Define o baud rate de comunicacao da placa com o ROS
  nh.getHardware()->setBaud(115200);

  //Inicia o sensor
  bmp180.begin();

  //Passa a publicar o topico e inicia o nó
  nh.advertise(temp1);
  nh.initNode();
}

void loop() {
  //Pega o valor do sensor para publicar no topico
  temperatura1.data = bmp180.readTemperature();

  //Publica no topico
  temp1.publish( &temperatura1);
  nh.spinOnce();
}
