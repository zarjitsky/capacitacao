#include <ros.h>
#include <std_msgs/UInt16MultiArray.h>
#include <std_msgs/Float32.h>
#include <Servo.h>

//Define os pinos
#define portaServo 6
int led = 5;

//Inicia a variavel do tipo servo
Servo servo1;

//Cria o callback para o subscriber do ROS
void messageCB( const std_msgs::UInt16MultiArray& controle){
  //Escreve a posicao recebida do topico no servo
  servo1.write(controle.data[0]);
  //Analiza o sinal recebido pelo topico para ligar ou desligar o LED
  if (controle.data[1] == 1){
    digitalWrite(led, HIGH);
  }
  else if (controle.data[1] == 0){
    digitalWrite(led, LOW);
  }
}

//Inicia o ROS
ros::NodeHandle nh;

//Define o subscriber de nome sub, para o topico capacitacao, com callback messageCB
ros::Subscriber<std_msgs::UInt16MultiArray> sub("capacitacao", &messageCB);

void setup() {
  //Define o baud rate para comunicar com o ROS
  nh.getHardware()->setBaud(115200);

  //Inicia o servo
  servo1.attach(portaServo);

  //Inicia o LED
  pinMode(led, OUTPUT);
  digitalWrite(led, HIGH);

  //Registra o subscriber e inicia o no
  nh.subscribe(sub);
  nh.initNode();
}

void loop() {
  //roda o ros
  nh.spinOnce();
}
