Qualquer duvida falar com Bruno Zarjitsky
brunozar@poli.ufrj.br

Instalando o rosserial

1 - Já tendo o ROS instalado, entre no seu workspace, clone o rosserial e compile

http://wiki.ros.org/rosserial

2 - Depois de compilado, vá para a pasta onde seu arduino mantem as bibliotecas
Provavelmente será algo do tipo

~/home/<nome do seu usuario>/Arduino/libraries

3 - Rode o comando que vai gerar a biblioteca do ROS no arduino pra voce
para rodar esse comando nao é preciso rodar o roscore

rosrun rosserial_arduino make_libraries.py .


Como rodar os codigos do arduino:

1 - Rodar o roscore

roscore

2 - Em um novo terminal, rodar o rosserial de acordo com a porta que o seu arduino está ligado
Nesse exemplo o arduino está na porta ttyUSB0

rosrun rosserial_python serial_node.py _baud:=115200 _port:=/dev/ttyUSB0

3 - Dar os comandos para o arduino 
Com o rostopic pub voce esta publicando uma mensagem no topico 
Voce poderia ter escrito rostopic pub /capacitacao, e ir dando tab que o proprio ROS iria

rostopic pub /capacitacao std_msgs/UInt16MultiArray ayout:
  dim:
  - label: ''
    size: 0
    stride: 0
  data_offset: 0
data: [0, 1]"

3 - Receber os dados do arduino
Com o rostopic echo voce ve algo que está sendo publicado em um topico
Com o comando 

rostopic list

Voce pode ver os topicos que estão sendo publicado 

rostopic echo /temperatura1


